import React, { Fragment } from "react";
import { Route } from "react-router-dom";

import "./App.css";
import Home from "./components/Home";
import ListContacts from "./components/contact/Contacts";
import AppHeader from "./components/AppHeader";

const App = () => (
  <Fragment>
    <AppHeader />
    <Fragment>
      <Route exact path="/" component={Home} />
      <Route path="/contacts" component={ListContacts} />
    </Fragment>
  </Fragment>
);

export default App;
