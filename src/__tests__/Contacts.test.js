import React from "react";
import { MemoryRouter } from "react-router-dom";
import { create, act } from "react-test-renderer";
import { MockedProvider } from "@apollo/react-testing";
import wait from "waait";

import Contacts from "../components/contact/Contacts";
import { GET_CONTACTS } from "../components/queries";

const mocks = [
  {
    request: {
      query: GET_CONTACTS
    },
    result: () => {
      return {
        data: {
          contacts: [
            {
              name: "Janet Doe",
              email: "jane@doe.com",
              id: "SZuuoBIHVRYuWGWavPYH2",
              created: 1574807101794,
              updated: 1574807101794
            },
            {
              id: "Ge9X8RnaJF0gCaihsF0EK",
              name: "Andrey Nikolov",
              email: "a@a.com",
              created: 1574807101794,
              updated: 1574936412473
            }
          ]
        }
      };
    }
  }
];

test("Snapshot testing Contacts", () => {
  const tree = create(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MemoryRouter>
        <Contacts />
      </MemoryRouter>
    </MockedProvider>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});

test("Contacts has a heading and a table", async () => {
  let component, instance;
  act(() => {
    component = create(
      <MockedProvider mocks={mocks} addTypename={false}>
        <MemoryRouter>
          <Contacts />
        </MemoryRouter>
      </MockedProvider>
    );
  });
  await wait(0);
  instance = component.root;
  const h4 = instance.findByType("h4");
  expect(h4.children).toContain("Contacts Manager");
  expect(instance.findAllByType("tr")).toHaveLength(3);
  expect(instance.findByType("tbody").findAllByType("tr")).toHaveLength(2);
  expect(instance.findAllByType("button")).toHaveLength(2);
  expect(
    instance.findAll(el => el.type === "a" && el.props.href === "/contacts/new")
  ).toHaveLength(1);
});
