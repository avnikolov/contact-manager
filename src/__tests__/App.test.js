import React from "react";
import ReactDOM from "react-dom";
import { MemoryRouter } from "react-router-dom";
import { shallow } from "enzyme";
import TestRenderer from "react-test-renderer";
import App from "../App";

test("App renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(
    <MemoryRouter>
      <App />
    </MemoryRouter>,
    div
  );
});

// Shallow Rendering
test("App renders shallow without crashing", () => {
  shallow(<App />);
});

test("Snapshot testing App", () => {
  const tree = TestRenderer.create(
    <MemoryRouter>
      <App />
    </MemoryRouter>
  ).toJSON();
  expect(tree).toMatchSnapshot();
});
