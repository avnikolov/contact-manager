import React from "react";
import { MemoryRouter } from "react-router-dom";
import { mount } from "enzyme";
import { MockedProvider } from "@apollo/react-testing";

import App from "../App";
import Home from "../components/Home";
import Contacts from "../components/contact/Contacts";
import ContactEditor from "../components/contact/ContactEditor";
import { GET_CONTACTS } from "../components/queries";

const mocks = [
  {
    request: {
      query: GET_CONTACTS
    },
    result: {
      data: {
        contacts: []
      }
    }
  }
];

test("Initial page is home", () => {
  const wrapper = mount(
    <MemoryRouter>
      <App />
    </MemoryRouter>
  );

  expect(wrapper.find(Home)).toHaveLength(1);
  expect(wrapper.find(Contacts)).toHaveLength(0);
});

test("/contacts points to contacts", () => {
  const wrapper = mount(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MemoryRouter initialEntries={["/contacts"]}>
        <App />
      </MemoryRouter>
    </MockedProvider>
  );

  expect(wrapper.find(Home)).toHaveLength(0);
  expect(wrapper.find(Contacts)).toHaveLength(1);
});

test("App has 2 links, and when you navigate to /contacts, Contacts page is displayed", () => {
  const wrapper = mount(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MemoryRouter>
        <App />
      </MemoryRouter>
    </MockedProvider>
  );
  const links = wrapper.find("a");
  expect(links).toHaveLength(2);
  wrapper.find("a[href='/contacts']").simulate("click");
  expect(wrapper.contains(Contacts)).toEqual(true);
});

test("navigate to /contacts/new", () => {
  const wrapper = mount(
    <MockedProvider mocks={mocks} addTypename={false}>
      <MemoryRouter initialEntries={["/contacts/new"]}>
        <App />
      </MemoryRouter>
    </MockedProvider>
  );
  expect(wrapper.contains(ContactEditor)).toEqual(true);
});
