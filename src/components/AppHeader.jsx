import React from "react";
import { Link } from "react-router-dom";
import {
  AppBar,
  Button,
  Toolbar,
  Typography,
  withStyles
} from "@material-ui/core";
import { Home, AccountBox } from "@material-ui/icons";

import Logo from "../assets/Logo";

const styles = {
  flex: {
    flex: 1
  },
  margin: {
    marginLeft: 25
  },
  logo: {
    width: 45,
    height: 45
  }
};

const AppHeader = ({ classes }) => (
  <AppBar position="static">
    <Toolbar>
      <Typography variant="h6" color="inherit">
        GQL
      </Typography>
      <div className={classes.margin}>
        <Button color="inherit" component={Link} to="/">
          Home <Home />
        </Button>
        <Button color="inherit" component={Link} to="/contacts">
          Contacts <AccountBox />
        </Button>
      </div>
      <div className={classes.flex} />
      <div className={classes.logo}>
        <Logo />
      </div>
    </Toolbar>
  </AppBar>
);

export default withStyles(styles)(AppHeader);
