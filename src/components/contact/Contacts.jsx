import React, { Fragment, useEffect } from "react";
import { withRouter, Route, Link, useLocation } from "react-router-dom";
import {
  withStyles,
  Typography,
  IconButton,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Fab,
  LinearProgress
} from "@material-ui/core";
import { Add, Edit, Delete } from "@material-ui/icons";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { GET_CONTACTS, DELETE_CONTACT } from "../queries";

import ContactEditor from "./ContactEditor";

const styles = () => ({
  posts: {
    marginTop: 16,
    height: 700,
    overflowY: "scroll"
  },
  fab: {
    position: "absolute",
    bottom: 18,
    right: 18
  }
});

const Contacts = ({ classes }) => {
  const renderContactEditor = (id, allContacts) => {
    const contact = allContacts.find(c => c.id === id);
    return <ContactEditor contact={contact} />;
  };

  const { loading, error, data, refetch } = useQuery(GET_CONTACTS, {
    fetchPolicy: "network-only"
  });
  const [deleteContact] = useMutation(DELETE_CONTACT, {
    onCompleted() {
      refetch();
    }
  });
  const location = useLocation();

  useEffect(() => {
    refetch();
  }, [location]);

  if (loading) return <LinearProgress variant="query" />;
  if (error) return <React.Fragment>Error :(</React.Fragment>;

  return (
    <Fragment>
      <Typography variant="h4">Contacts Manager</Typography>
      {data.contacts && data.contacts.length > 0 ? (
        <Paper elevation={1} className={classes.posts}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Name</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Created</TableCell>
                <TableCell>Updated</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.contacts &&
                data.contacts.map(contact => (
                  <TableRow key={contact.id}>
                    <TableCell>{contact.name}</TableCell>
                    <TableCell>{contact.email}</TableCell>
                    <TableCell>
                      {new Date(contact.created).toLocaleString()}
                    </TableCell>
                    <TableCell>
                      {new Date(contact.updated).toLocaleString()}
                    </TableCell>
                    <TableCell align="center">
                      <IconButton
                        component={Link}
                        to={`/contacts/${contact.id}`}
                        color="inherit"
                      >
                        <Edit />
                      </IconButton>
                      <IconButton
                        onClick={() => {
                          if (
                            window.confirm(
                              `Are you sure you want to delete "${contact.name}"`
                            )
                          ) {
                            deleteContact({ variables: { id: contact.id } });
                          }
                        }}
                        color="inherit"
                      >
                        <Delete />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Paper>
      ) : (
        <Typography variant="subtitle1">No contacts to display</Typography>
      )}
      <Fab
        color="secondary"
        aria-label="add"
        className={classes.fab}
        component={Link}
        to="/contacts/new"
      >
        <Add />
      </Fab>
      <Route
        exact
        path="/contacts/:id"
        render={props =>
          renderContactEditor(props.match.params.id, data.contacts)
        }
      />
    </Fragment>
  );
};

export default withRouter(withStyles(styles)(Contacts));
