import React from "react";
import { withRouter } from "react-router-dom";
import { useMutation } from "@apollo/react-hooks";
import {
  withStyles,
  Card,
  CardContent,
  CardActions,
  Modal,
  Button,
  TextField
} from "@material-ui/core";
import { Form, Field } from "react-final-form";

import { ADD_CONTACT, UPDATE_CONTACT } from "../queries";

const styles = () => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  modalCard: {
    width: "90%",
    maxWidth: 500
  },
  modalCardContent: {
    display: "flex",
    flexDirection: "column"
  },
  marginTop: {
    marginTop: 16
  }
});

const mustBeString = value =>
  !/^[a-zA-Z-\s]+$/.test(value) ? "Invalid Name" : undefined;
const mustBeEmail = value =>
  !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value) ? "Invalid Email" : undefined;

const PostEditor = ({ classes, contact, history }) => {
  const [updateContact] = useMutation(UPDATE_CONTACT, {
    onCompleted() {
      history.goBack();
    }
  });
  const [addContact] = useMutation(ADD_CONTACT, {
    onCompleted() {
      history.goBack();
    }
  });

  const onSave = contact => {
    if (contact.id) {
      // edit contact
      contact.updated = Date.now();
      const { __typename, ...uContact } = contact;
      updateContact({ variables: { contact: uContact } });
    } else {
      // new contact
      contact.created = Date.now();
      contact.updated = Date.now();
      addContact({
        variables: { contact }
      });
    }
  };

  return (
    <Form initialValues={contact} onSubmit={onSave}>
      {({ handleSubmit }) => (
        <Modal className={classes.modal} onClose={() => history.goBack()} open>
          <Card className={classes.modalCard}>
            <form onSubmit={handleSubmit}>
              <CardContent className={classes.modalCardContent}>
                <Field name="name" placeholder="Name" validate={mustBeString}>
                  {({ input, meta }) => (
                    <TextField
                      label="Name"
                      required
                      error={meta.error && meta.touched}
                      helperText={meta.error && meta.touched && meta.error}
                      autoFocus
                      {...input}
                    />
                  )}
                </Field>
                <Field name="email" placeholder="Email" validate={mustBeEmail}>
                  {({ input, meta }) => (
                    <TextField
                      required
                      className={classes.marginTop}
                      error={meta.error && meta.touched}
                      helperText={meta.error && meta.touched && meta.error}
                      label="Email"
                      {...input}
                    />
                  )}
                </Field>
              </CardContent>
              <CardActions>
                <Button size="small" color="primary" type="submit">
                  Save
                </Button>
                <Button size="small" onClick={() => history.goBack()}>
                  Cancel
                </Button>
              </CardActions>
            </form>
          </Card>
        </Modal>
      )}
    </Form>
  );
};

export default withRouter(withStyles(styles)(PostEditor));
