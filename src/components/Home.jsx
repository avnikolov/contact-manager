import React from "react";
import { Paper, Typography, withStyles } from "@material-ui/core";

const styles = {
  paper: {
    width: 350,
    height: 80,
    margin: 15,
    padding: 15,
    textAlign: "left"
  }
};

const Home = ({ classes }) => (
  <Paper className={classes.paper}>
    <Typography variant="h5" component="h3">
      Home page for GQL
    </Typography>
    <Typography component="p">Created by anonymous</Typography>
  </Paper>
);

export default withStyles(styles)(Home);
